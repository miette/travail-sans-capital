---
layout: layouts/home.html
---

# On se lève et on se casse
Tour d'horizon d'un travail sans capital

## J'aime mon travail mais...

- je voudrais le faire mieux
- je vais au travail la boule au ventre
- je n'ai plus d'enthousiasme
- je me sens coincée
- je n'y trouve plus de sens...

## Un autre monde du travail existe

Nous sommes celles et ceux qui refusent un monde du travail qui abîme physiquement ou mentalement les travailleuses et travailleurs.

Nous ne continuerons pas à le servir, et nous aiderons toutes celles et ceux qui souhaitent en partir.

## Quelle est votre situation ?

- [Évaluer ma situation au travail](pages/ma-situation-de-travail)
- [Clarifier ce qui est important pour moi](pages/ce-qui-est-important)
- [Je veux changer mais j'ai peur](pages/changer-mais-j-ai-peur)
- [Quelle structure choisir ?](pages/quelle-structure-choisir)
