---
title: Qui décide pour la structure ?
description: |
  Stratégie, budget, décisions engageantes.

sections:
- answers:
    Patrons actionnaires: [salariat, zoneProbleme]
    Patrons: [salariat, zoneProbleme]
    Moi: [micro-entreprise, portage salarial, CAE, zoneEnthousiasme]
    Conseil d'Administration: [coop, CAE]
    Tous·tes les travailleur·ses: [coop, CAE, zoneEnthousiasme]

tags:
- évaluer
- clarifier
---

La pub [c'est qui le patron ?](https://www.youtube.com/watch?v=DRIuRR8jSSM).

Les décisions qui t'impactent, c'est toi qui les prends. À égalité avec les autres. Voire même les propositions viennent des personnes qui de soi.

Vas-y, parle-nous du plan climat de ton agglomération. "On" m'a dit OK de faire passer des dépenses d'impression de tracts en note de frais. C'est génial parce qu'on utilise un outil du capital pour avoir plus de moyens pour militer. 100€ pour l'asso c'est rien. De ma poche, ça représentait davantage.
Y'a pas eu de discussion : est-ce que je peux faire ça, yes ok vas-y.

J'ai filé des sous a des assos, je décide tout seul pour moi-même, je prend autant de vacances que je veux, je travaille où je veux.

La prime mobilité, donc si je justifie d'un nombre de trajets à vélo (déclaratif) prime de 500€

Tout ce qui est rapport au travail, c'est des notes de frais.

Maïtané va faire passer Zelda en note de frais.
