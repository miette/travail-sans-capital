---
title: Rechercher des projets facturés
sections:
- title: Aisance
  answers:
    Au secours ça fait peur: []
    J'y connais rien: []
    Tranquille: []
- title: Responsabilité
  answers:
    C'est pas mon problème: [portage salarial, micro-entreprise]
    Collective: [coop, CAE]
    J'ai pas à m'en occuper: [coop, salariat]
tags:
- clarifier
---
Y'a des personnes plus ou moins à l'aise.\
Y'a pas d'obligation de faire ça tout·e seule.\
Ça se teste aussi.

Si de l'argent doit rentrer tout de suite, des gens autour aident à chercher, vous proposent des contrats/du travail,  vous invitent sur des projets, et/ou c'est apporté par les gens avec qui vous vous associez (dans le cas d'une coop).

Il faut décoreller le fait d'avoir un salaire payé, et travailler dès le début pour un·e client·e qu'on a trouvé nous-même. Si la structure a de l'argent d'avance, ça peut servir d'amorce, se mettre à tout petit temps partiel, avoir des économies (on en reparle dans 2 slides)
