---
title: Besoin de sécurité
sections:
- title: Ressenti perso
  answers:
    Burn-out: [zoneDanger]
    J'ai la boule au ventre: [zoneDanger]
    Ça dépend des jours: [zoneProbleme]
    On fait équipe/soutien de l'équipe: [zoneEnthousiasme]
- title: Position structure
  answers:
    On m'enfonce: [coop, salariat, zoneDanger]
    Y'a des petites remarques: [coop, salariat, CAE, zoneProbleme]
    On ne s'occupe pas de moi: [coop, salariat, CAE, micro-entreprise, portage salarial, zoneProbleme]
    On fait attention à moi: [coop, salariat, CAE, zoneEnthousiasme]
    On m'aide à aller mieux: [coop, CAE, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---

Puisqu'on choisit les gens\
Puisqu'on choisit les projets\
Puisqu'on choisit le sens\
Et bien… tout ça, ça contribue au fait que les personnes de la structure aillent bien, et tendent le plus possible vers la droite de la jauge.

On en revient au savoir-être.

Ces 2 indicateurs sont un thermomètre pour savoir où t'en es, et où en est la dynamique de la structure.
