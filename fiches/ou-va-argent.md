---
title: Où va l'argent ?
description: |
  À qui profitent les bénéfices ?
tag: Thune
sections:
- answers:
    Patrons actionnaires: [salariat, zoneProbleme]
    Patrons: [salariat, zoneProbleme]
    Moi: [micro-entreprise, portage salarial, coop, CAE, zoneEnthousiasme]
    Tous·tes les travailleur·ses/structure:
      choices: [coop, CAE, zoneEnthousiasme]
      description: Et ça y reste.
tags:
- évaluer
- clarifier
---


Saviez-vous que tous les jours que les employés d'une entreprise du CAC 40 vont travailler d'ici le 31 décembre seront fait pour payer leurs actionnaires ? (depuis le 22 septembre = 3 mois et 8 jours)

Et vous ? A partir de quand est-ce que votre travail ne paie plus votre salaire mais les revenus de votre/vos patrons ?


Coop : Tandis que nous ça paie nos salaires + fonctionnement de la structure (coûts réduits au minimum)

CAE : Obliger de se reverser résultat sous forme de salaire
Tous les bénéfices de mon activité : pour ma pomme et/ou je redonne. Notamment en prenant quelqu'un·e en alternance.

Du coup on garde de l'argent pour les mois où l'on facture pas, de façon subie ou choisie.

Et si il y a trop d'argent ?\
On décide ensemble de ce qu'on en fait :

- Investissements au bénéfice des gens de la structure
- Formation
- Aider les autres travailleureuses
- Bosser avec des personnes qui vont moins facturer / des projets qui ont pas de moyens
- Faire des projets cools :
  - Faiseuses du Web ?
  - Acheter des forêts
  - Filer à des assos militantes

Ajoute des marges de manœuvre plutôt que d'enrichir des personnes en particulier.


_Note_\
https://twitter.com/JLMelenchon/status/1573801964101685248

>Tous les salariés de ce pays, depuis le 22 septembre, travaillent pour une seule chose : pour payer les dividendes des actionnaires. Ce n'est pas supportable. (…) En dix ans, ils ont capté 45 jours supplémentaires.
> — https://www.marianne.net/economie/jour-du-depassement-capitaliste-a-partir-de-ce-22-septembre-vous-travaillez-pour-les-actionnaires
