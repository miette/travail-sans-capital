---
title: Chômage, santé et protection sociale
sections:
- answers:
    LOL: [micro-entreprise, zoneDanger]
    Couvert·e: [salariat, portage salarial, CAE, coop, zoneEnthousiasme]
    Bien couvert·e: [salariat, portage salarial, CAE, coop, zoneEnthousiasme]
- title: Solidarité nationale
  description: Mutualisation = solidarité
  answers:
    Contribue le - possible: [zoneDanger]
    Contribue à fond: [zoneEnthousiasme]
tags:
- évaluer
- clarifier
---

C'est problématique qu'on se serve de statuts où les personnes n'ont quasiment aucune protection sociale — si tomber malade te met en danger professionnellement, c'est une forme de précarité et d'injustice. (#Deliveroo #UberEats) <-> intérimaire

On peut faire des choix : des mutuelles qui rembourse des séances de psy, payer le maximum possible, voire compenser par des primes (par exemple, pour renouveler un appareillage auditif).

Plus on côtise, mieux c'est :

- On côtise à hauteur de ce qu'on génère comme valeur
- On bénéficie d'aides à hauteur de ce dont on a besoin
- Avec un système de retraite par répartition, plus on côtise, plus on peut payer de retraites (et donc éviter de repousser l'âge de départ…)
