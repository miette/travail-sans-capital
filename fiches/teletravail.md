---
title: Télétravail
sections:
- answers:
    Nope: [salariat, zoneEnnui]
    Négocié: [salariat, zoneEnnui]
    Fixé: [salariat]
    Détermination par le collectif: [coop]
    Libre: [portage salarial, CAE, coop, micro-entreprise, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---

<!-- De gauche à droite, une flèche qui dit "Merci le Covid <3" -->
