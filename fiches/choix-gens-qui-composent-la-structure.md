---
title: Le choix des gens qui composent la structure
sections:
- answers:
    Patron: [salariat, zoneEnnui]
    Le collectif intégré à la structure: [portage salarial, CAE, coop, zoneEnthousiasme]
    Tout le monde: [coop, zoneEnthousiasme]
tags:
- clarifier
---

<!-- Pourquoi portage salarial est là ? -->
<!-- A la relecture, je trouv pas ça clair "le collectif intégré à la structure"… "une partie de l'équipe" ça serait ptet + clair ? -->
