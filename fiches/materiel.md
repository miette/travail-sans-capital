---
title: Matériel
sections:
- answers:
    Fixé: [salariat, zoneEnnui]
    À la demande: [salariat, zoneEnnui]
    Décidé en collectif: [coop]
    Libre: [portage salarial, CAE, coop, micro-entreprise, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
