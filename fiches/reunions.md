---
title: Réunions
sections:
- answers:
    Qu'est-ce que je fous là: [zoneDanger]
    On tourne en rond / C'est dur d'en placer une: [zoneProbleme]
    Ça manque de méthode: [zoneEnnui]
    On repart avec le sourire: [zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
