---
title: Gérer l'administratif
sections:
- title: Aisance
  answers:
    Au secours ça fait peur: []
    Je veux bien essayer: []
    Les doigts dans le nez: []
- title: Responsabilité
  answers:
    Individuelle: [portage salarial, micro-entreprise]
    _Individuelle: [CAE]
    Collective: [coop]
    Pas à m'en occuper: [coop, salariat]
- title: Quantité
  answers:
    Pas mal: [coop]
    _Pas mal: [coop]
    _Rien: [CAE, portage salarial, micro-entreprise]
    Rien: [salariat]
tags:
- clarifier
---

Vous êtes à l'aise combien ?

Pour ne pas gérer d'administratif :

- en coopérative y'a moyen de partager, de binomer avec quelqu'un·e.
- En CAE y'a quasiment rien à gérer. J'émets des factures et je remplis des notes de frais. Et en plus on est accompagné·e, selon notre expérience.
