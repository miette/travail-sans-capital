---
title: Niveau de rémunération
sections:
- answers:
    «Négocié»: [salariat, zoneProbleme]
    Fixé par une grille: [salariat, coop]
    Déterminé par le collectif: [coop]
    Pleinement choisi: [portage salarial, CAE, coop, micro-entreprise, zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
