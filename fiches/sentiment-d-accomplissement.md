---
title: Sentiment d'accomplissement
sections:
- answers:
    Ce que je fais est inutile: [zoneDanger]
    Je me fais chier / J'ai rien à faire: [zoneProbleme]
    Rien à signaler: [zoneEnnui]
    Ça dépend des jours: []
    Je m'éclate dans ce que je fais: [zoneEnthousiasme]
tags:
- évaluer
- clarifier
---
