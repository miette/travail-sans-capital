# Glossaire

## Actionnaire

Personne qui détient un certain pourcentage du [capital](#capital) d'une entreprise.\
Son [pouvoir (de vote)](#pouvoir) est proportionnel au pourcentage détenu.\
Seul·e ou par alliance, si cette proportion atteint au moins 51% des parts, cette personne/alliance décide comme elle l'entend.

Un·e actionnaire peut être une autre entreprise/entité morale.

## Administratif

## AE (auto-entreprise) / EI (entreprise individuelle) / ME, micro (micro-entreprise)

## Aisance

## Argent

Quelque chose qui prend beaucoup de place mais dont on parle peu.\
Quelque chose qu'on génère beaucoup mais dont on bénéficie peu.\
Quelque chose qui fait que d'autres qui en possèdent plus que nous décident à notre place.

## Besoin de sécurité

## Burn-out / syndrôme d'épuisement professionnel

## CA (Conseil d'Administration)

## CAE (Coopérative d'Activité et d'Entrepreneuriat, et/ou Coopérative d'Activité et d'Emploi)

## Capital

Plusieurs choses sont impliquées par le capital dans une entreprise :

- un [pouvoir de décision](#pouvoir) : 
- une rémunération

## Consentement

C'est quand on se dit "oui" explicitement.

En réalité, "qui ne dit rien, ne consent pas vraiment".

## Coop / Coopérative

## Décision

## Décision engageante

## Déclaratif

## Finances

## Gérant·e

## Gouvernance

## Gouvernance partagée

## Modalités de prise de décision

## Nope

Y'a pas moyen. Certainement pas.

## Patron·ne

Personne qui dispose de la majorité du capital d'une entreprise, _et_ qui pilote sa gestion.\
On dénomme "patron" une personne qui a tout [pouvoir](#pouvoir).

Une personne qui _pilote_ la gestion mais qui n'est pas [actionnaire](#actionnaire) majoritaire n'a pas autant de [pouvoir](#pouvoir) — elle prend elle-même ses ordres stratégiques des autres actionnaires.

## Pouvoir

## Protection sociale

## Rémunération

## Rythme (de travail)

## Salaire

[Argent](#argent) perçu en contrepartie de son [travail](#travail). C'est le fruit direct de notre travail.\
Tout salaire perçu implique un contrat de travail.

Le saviez-tu ? Recevoir une fiche de paie fait office de contrat de travail implicite, même si on n'en a pas signé.

## Santé

## Santé mentale

## Sociétaire

Personne qui détient un certain pourcentage du [capital](#capital) d'une [coopérative](#coop-coopérative).\
Son [pouvoir (de vote)](#pouvoir) n'est pas lié au 

En général, 1 sociétaire = 1 personne = 1 voix.

## Solidarité

## Solidarité nationale

## Structure

## Subordination

## Temps libre

## Travail

## Travailleur·se

## Unilatéral

## Vécu
