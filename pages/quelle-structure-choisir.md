---
---

# Quelle structure choisir ?

Il existe des structures qui sont conçues pour prendre soin des personnes, que nous appelons "les coopératives". Nous nous limiterons à elle parce que nous voulons construire un monde du travail conçu pour prendre soin des travailleuses et travailleurs


Nous les séparons en 2 catégories selon votre situation :
- Vous êtes seul·e
- Vous êtes plusieurs


## Si vous êtes seul·e : La CAE

La CAE (Coopérative d'Activité et d'Emploi) est une coopérative accueillant les personnes qui veulent monter leur activité.

### Les avantages
- Vous serez salarié.e en CDI
- Vous serez accompagné.e sur les aspects comptables et administratifs
- Vous pouvez facturer et toucher le chômage pour démarrer en douceur
- Vous pouvez vous impliquer dans la vie de la CAE....ou pas !

### Les inconvénients
- Il faut trouver la CAE avec les règles qui vous conviennent
- Vous devez être à l'équilibre entre l'argent que vous faites rentrer dans la CAE et le salaire que vous vous payez.

### Choisir sa CAE

Quelques critères pour choisir sa CAE :
- Locale ou nationale ?
- Avec des locaux pour aller y travailler ?
- Centrée sur un domaine d'activité ou accueillant une diversité d'activités ?
- Organisme de formation ou pas ?
- L'existence d'un esprit de communauté / de partage au sein de la CAE

et ensuite [il faut prendre contact](https://www.les-cae.coop/trouver-une-cae-0) pour voir si le courant passe bien !


## Si vous êtes plusieurs : Monter sa coopérative

En montant une coopérative, c'est vous qui choisissez les règles de fonctionnement !

### Les avantages

- Votre coopérative, vos règles
- Construire une structure avec des personnes que vous appréciez
- Tout ne dépend pas de vous, vous pouvez vous reposez sur les autres

### Les inconvénients

- Nécessite de définir des règles communes
- Nécessite de très régulièrement vérifier que l'on est d'accord
- Nécessite de savoir s'écouter et gérer les conflits

### Monter sa coopérative

Nous vous proposons de copier le modèle associatif de l'Échappée Belle parce que c'est celui que nous connaissons pour être rapide et simple à monter. Si vous préférez creuser le sujet, n'hésiter pas à vous renseigner sur les autres formes : les SCOP notamment.

1. Pour démarrer, vous pouvez copier les statuts de [L'Échappée Belle](https://lechappeebelle.team/) et voir ce qui vous convient ou non.
N'hésitez pas à lire [la petite note clarifiant les risques](https://github.com/lechappeebelle/exploration/issues/16#issuecomment-570787264) si vous conservez les statuts tel quels.

2. Une fois que les statuts vous conviennent, [créez l'association](https://www.service-public.fr/particuliers/vosdroits/R1757) en envoyant 3 documents :

- Procès-verbal de l'assemblée constitutive ou son extrait daté et signé, portant le nom et le prénom du signataire
- Statuts de l'association datés et signés par au moins 2 personnes mentionnées sur la liste des dirigeants (bureau ou conseil d'administration), portant leur nom, prénom, et fonction au sein de l'association
- Mandat (le cas échéant) portant la signature, le nom, le prénom et la fonction au sein de l'association de l'une des personnes chargées de l'administration (bureau ou conseil d'administration)


Une fois l'association créée,

1. Ouvrez un compte en banque
2. Choisissez une assurance (nous on a pris la MAIF)
3. Choisissez une mutuelle (nous on a pris Wemind)
4. Choisissez un cabinet comptable pour la compta et le social (Pour le social il est sûrement possible de passer [par le CEA](https://www.cea.urssaf.fr/portail/accueil/s-informer-sur-offre-de-service/essentiel-du-cea.html) mais jusqu'à présent on a eu la flemme)

<!--Je crois qu'il manque une mention sur la convention collective…-->




**[La bibliographie/liste des ressources avec tous les liens qui vont bien](https://app.flus.fr/collections/1743346650628061722)**
