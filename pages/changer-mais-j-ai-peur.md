---
---

# Je veux changer mais j'ai peur

Bienvenue !

Bravo pour être arrivé·e jusqu'ici.

Les pages précédentes ne sont pas facile et demandent pas mal d'introspection, on reconnaît que ça peut secouer.

Ici, on va essayer de clarifier quelles sont les thématiques qui peuvent être effrayantes pour certain·es.

L'idée c'est de les lister, de se positionner et de voir quelles actions sont possibles pour vous aider si ce sont des choses qui vous mettent mal à l'aise (ou qui vous font carrément flipper).


## Niveau d'énergie

- [Niveau d'énergie](/fiches/niveau-energie)

Si vous n'avez pas d'énergie, **il est primordial de vous reposer avant toute chose.**

- Un médecin peut vous donner un arrêt maladie,
- ou vous pouvez profiter d'une période de chômage pour prendre soin de vous.
- Vous pouvez également demander un temps partiel à votre employeur pour vous dégager du temps.

Si vous n'avez pas de médecin à votre écoute, **trouver un·e professionnel·le bientraitant·e** peut être votre première action.

**Prendre soin de vous, c'est LA priorité.**


## Gestion de l'administratif

- [Gestion de l'administratif](/fiches/gestion-administratif)

Si vous avez peur de l'administratif, peut-être pouvez-vous
- trouver une structure qui vous accompagne (les CAE sont là pour ça)
- discuter du sujet avec quelqu'un qui gère déjà l'administratif du type de structure que vous visez, pour vous rendre compte de ce que cela représente,
- monter une coopérative avec quelqu'un qui adore la paperasse,
- vous former sur le sujet, seul·e ou avec un·e ami·e


## Parler d'argent

Chacun·e a une relation à l'argent particulière, mais nous croyons que clarifier la situation peut déjà aider grandement.

Nous avons donc dédié la page ["De combien j'ai besoin pour vivre"](/pages/de-combien-j-ai-besoin-pour-vivre) à cette question, et y avons rajouté quelques liens vers d'autres articles bien écrits sur le sujet.

## Recherche de projets facturés

- [Recherche de projets facturés](/fiches/recherche-projets-factures)

Si vous ne savez pas comment trouver des clients, vous pouvez
- demander à d'autres personnes comment elles ont fait,
- lancer une petite annonce ou
- un mail aux personnes avec qui vous aimeriez travailler, ou dont vous aimez la façon de travailler ?

Là encore, si ça ne vous met pas à l'aise du tout, la meilleure réponse est de rejoindre un collectif qui est habitué à le faire et vous mettra le pied à l'étrier. Par exemple en rejoignant une CAE !


## Conclusion : Votre niveau de prise de risque

- [Prise de risque](/fiches/prise-de-risque)

### Faire l'état des lieux

Pour y voir le plus clair possible, nous vous proposons de noter en 2 listes :

- Vos "forces" qui diminuent la prise de risque
- Vos "fragilités" qui nécessite des précautions supplémentaires face au risque

Voici quelques exemples :

Quels sont mes besoins financiers ?
- Loyer / prêt
- Pouvez-vous le réduire si nécessaire ?
(on vous a préparé un beau [tableau à remplir](https://framagit.org/miette/travail-sans-capital/-/raw/main/ressources/tableur_budget_perso.ods) pour mettre ça au clair, rendez-vous sur la page ["De combien j'ai besoin pour vivre"](/pages/de-combien-j-ai-besoin-pour-vivre) pour toutes les questions d'argent)

Est-ce que des personnes dépendent de moi ?
- enfant(s)
- conjoint·e
- famille
- proche
- groupe de travail/association


Est-ce que je peux demander de l'aide/trouver des ressources auprès de gens ?
- Couple
- Parent(s)
- Ami·es
- Famille

Est-ce que j'appartiens à un réseau ?
- est-ce qu'il y a des clients là où je vis ?
- est-ce que je connais des personnes qui sauraient me recommander/coopter/me présenter aux bonnes personnes ?

Est-ce que vous connaissez le domaine d'activité dans lequel vous voulez travailler ?

- Je n'y connais pas grand chose
- J'ai des contacts qui peuvent me mettre le pied à l'étrier
- Je gère déjà en partie
- Je gère

Est-ce que le démarrage de votre activité demande des investissements ?
- Oui, et ils sont importants
- Oui, mais ils ne sont pas très importants
- Oui, mais je peux avoir des aides
- Oui, mais je peux les planifier
- Non

### Les actions possibles

Maintenant que vous voyez clairement vos forces et vos fragilités, l'idée est de voir comment faire pencher la balance en votre faveur, jusqu'à que vous vous sentiez assez en sécurité pour vous lancer.

Seul ou avec de l'aide extérieure, vous pouvez observer à présent les points qui vous freinent et chercher les marges de manoeuvre dont vous disposez.

Par exemple :

- Si vos besoins financiers sont importants
  - demander à votre entourage si il peut
    - vous aider financièrement,
    - vous héberger pour économiser un loyer...
  - vérifier si vous avez le droit au chômage ou a des aides de l'administration (les CAE sauront vous conseiller à ce propos)

- Si des personnes dépendent de vous
  - Pouvez-vous avoir de l'aide le temps de lancer votre activité ? (il faut compter a minima 1 an, 2 pour arriver à un rythme de croisière)
  - Si il s'agit de jeunes enfants, peut-être est-il possible d'attendre que les enfants soient plus autonomes ?

- Si vous ne vous sentez pas faire partie d'un réseau, vous pouvez
  - commencer à rejoindre des évènements où vous rencontrerez vos futurs pairs
  - poser des questions à des personnes que dont vous appréciez le travail

- Si votre activité demande des investissements, vous pouvez
  - Voir s'il est possible de les découper pour les lisser sur un temps long
  - Voir si vous pouvez avoir des aides financières pour ce matériel

## Pour finir

Maintenant que vous avez mis à plat tout ce qui pouvait vous mettre mal à l'aise, entre

- la gestion de l'administratif
- parler d'argent
- la recherche de projets facturés

et l'ensemble de vos forces et faiblesses, vous pouvez faire le bilan :

Vous sentez vous assez robuste pour sauter le pas ?

Si oui, rendez-vous sur la page [Choisir ma structure](/pages/quelle-structure-choisir)

Si non, vous pouvez décider des actions à mettre en place pour atteindre un équilibre suffisant.

Nous insistons : si vous n'avez pas beaucoup d'énergie en ce moment, il est important de se reposer, tout sera plus simple ensuite (pour des idées de comment prendre du repos, vous pouvez remonter en haut de la page !)

Si vous avez d'autres soucis, nous espérons que les différentes propositions d'actions de la page vous aurons donné des idées. Vous pouvez en faire une liste à part, **votre liste** et si cela vous aide, mettre en place des échéances pour bien visualiser le chemin qu'il vous reste à parcourir avant de refaire le bilan.

Bonne route !
