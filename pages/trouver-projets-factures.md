---
---

# Trouver des projets facturés

Alors là on va pas vous mentir, c'est le nerf de la guerre !

## Ce qu'on ne vous recommande pas

On ne vous recommande pas…
- D'aller sur des plateformes qui mettent prétendument en contact les freelances et des porteurs de projet… on retombe très rapidement dans une course au TJM le plus bas, et donc dans une mise en concurrence totalement contre-productive
- De faire du marketing de soi sur des "réseaux sociaux professionnels", là aussi ça va ressembler à une mise en concurrence et vous devrez prouver à quel point vous êtes plus mieux que les autres…

## Ce qui marche

Autour de nous, quand on demande « comment tu as trouvé tes clients » la réponse est systématiquement « le réseau », comprendre "le bouche à oreille"

Alors on est conscient·es que quand on se pose la question de trouver des projets, c'est pas une réponse qui est très "actionnable"
