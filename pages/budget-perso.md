---
---

# Budget personnel

Pour chaque ligne, vous pouvez :
- Lister vos dépenses actuelles
- si pertinent, lister vos dépenses dans une projection du futur (peut-être que si vous changez de boulot, vous ne devrez plus faire 50 km de voiture tous les jours ?)

Puis une fois le tableau rempli :
- Faire la somme de vos dépenses
- Si vous avez d'autres sources de revenus, faire la différence entre vos dépenses et les revenus

Si vous préférez, il y a un [tableur déjà tout prêt](https://framagit.org/miette/travail-sans-capital/-/raw/main/ressources/tableur_budget_perso.ods)

## Habitation
### Logement
Loyer (+ charges)
Prêt (+ charges)
Assurance logement
Impôts fonciers
Autre

### Énergie
Gaz
Électricité
Eau
Bois ou granules
Autre

### Abonnement
Internet
Forfait téléphone
Autre

### Aménagements
Meubles / Électroménager
Travaux / entretien
Jardin
Autre

### Banque / Assurances
Frais bancaires
Assurance personnelle (si différente du logement)
Prêt conso ou travaux
Autre

## Alimentation
Courses
Restaurants
Bar / Alcool
Autre

## Transport
### Véhicule personnel
Essence
Réparations / Entretien
Assurance
Autoroute
Prêt
Autre

### Transport en commun
Quotidien
Train
Bus
Avion
Bateau
Taxi, VTC
Covoiturage
Autre

## Animaux (domestiques)
Nourriture
Soins
Équipement
Assurance
Mutuelle
Autre

## Loisirs
### Vacances
Logement
Trajet
Autre

### Loisirs
Sport (matériel) / abonnement
Cinéma / Théâtre
Concert
Abonnements revues /plateforme streaming (musique / films)
Achats livres/ cd / DVD, etc.
Informatique / Hi-Fi
Habillement
Cadeaux (proches, etc.)
Dons / Cotisations (à un mouvement, une assoc etc)
Autre

## Santé
Frais de santé annexes
Hygiène
Mutuelle
Reste à charge sécu, participation aux actes
Médicaments/ accessoires non remboursés
Coiffeur, soins, esthétique
Tabac
Aide à domicile
Autre

## Enfants
Vêtements / Couches
Cantine
Jouets
Frais de garde (crèche / assistante maternelle / garderies / babysiter )
Pension "alimentaire"
Fournitures scolaires
Assurance scolaire
Cours de soutien
Scolarité / Prêt étudiant
Argent de poche
Loisirs / activités (sport, musique, …)
Abonnements (téléphone / revues)
Autre

## Revenus
Allocations / aides
Dons
Rentes / Loyers
Pension "alimentaire"
Autre
