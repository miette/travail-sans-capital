---
---

L'idée ici c'est de clarifier **ce qui vous convient ou ne vous convient pas/plus dans votre situation de travail actuelle.**


Pour l'instant le site n'est pas finalisé. On vous invite donc à ouvrir chaque page et à vous positionner sur les différents critères (idéalement en les redessinant sur un papier chez vous, pour avoir une bonne image générale)

{% include 'fiches-list.html', tag: 'évaluer' %}
