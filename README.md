# On se lève et on se casse 🚶🏻‍♀️ tour d’horizon d’un travail sans capital

Un spectacle animé par Maïtané et Thomas à Paris Web 2022 :

- [Présentation de 50 minutes](https://www.paris-web.fr/2022/conferences/on-se-leve-et-on-se-cassetour-dhorizon-dun-travail-sans-capital.php) ([les slides][slides])
- [Atelier de 1 heure 30 minutes](https://www.paris-web.fr/2022/ateliers/on-se-leve-et-on-se-casse-tour-dhorizon-dun-travail-sans-capital-latelier.php)

[Un **glossaire** pour mieux comprendre](GLOSSAIRE.md).\
[Une **bibliographie** par ici](https://app.flus.fr/collections/1743346650628061722).

Un [timer] pour suivre le timing des 50 minutes au fil de l'eau.

## Grille de lecture

- [Qui choisit les projets/clients ?](fiches/qui-choisit-les-projets.md)
- [Qui décide pour la structure ?](fiches/qui-decide-pour-la-structure.md)
- [Modalités de prise de décision](fiches/modalites-de-prise-de-decision.md)
- [Où va l'argent ?](fiches/ou-va-argent.md)
- [Choix des gens avec qui je travaille](fiches/choix-gens-avec-qui-je-travaille.md)
- [Seul·e / à plusieurs](fiches/seule-plusieurs.md)
- [Rythme de travail](fiches/rythme-de-travail.md)
- [Temps libre](fiches/temps-libre.md)
- [Chômage / santé / protection sociale](fiches/chomage-sante-protection-sociale.md)
- [Besoin de sécurité](fiches/besoin-securite.md)
- [Gestion de l'administratif](fiches/gestion-administratif.md)
- [Recherche de projets facturés](fiches/recherche-projets-factures.md)
- [Finances](fiches/finances.md)
- [Prise de risque](fiches/prise-de-risque.md)

En bonus, hors-présentation :

- [Accès à l'information](fiches/acces-a-l-information.md)
- [Accompagnement](fiches/accompagnement.md)
- [Matériel](fiches/materiel.md)
- [Méthodologie des projets](fiches/methodologie-des-projets.md)
- [Multi activité](fiches/multi-activite.md)
- [Niveau de rémunération](fiches/niveau-remuneration.md)
- [Relation au risque](fiches/relation-au-risque.md)
- [Le choix des gens qui composent la structure](fiches/choix-gens-qui-composent-la-structure.md)
- [Congés / Jours chômés](fiches/conges.md)
- [Formation et apprentissages](fiches/formation-apprentissages.md)
- [Gestion des conflits](fiches/gestion-de-conflits.md)
- [Humour](fiches/humour.md)
- [Réunions](fiches/reunions.md)
- [Sentiment d'accomplissement](fiches/sentiment-d-accomplissement.md)
- [Télétravail](fiches/teletravail.md)

## Rien à voir mais… pourquoi MIETTE ?

Parce qu'on a fait des sessions de travail chez [Miette](https://www.miettedinan.com/).\
Et qu'on pouvait en faire des acronymes :

- Mouvement Interprofessionnel d'Entreprises Très Très Empathiques
- Mieux Intéragir Entre Travailleuses et Travailleurs Éthiques
- Moult Injonctions d'Entreprises Terrifiantes voire Troublantes ou Ennyeuses

## Pour assembler les slides

Le résultat produira le fichier [`slides/index.pdf`](slides/index.pdf).

```bash
cpdf -merge $(find ./slides -type f -name '*.pdf' \( ! -name 'index.pdf' 
\)  | sort -k1) AND -presentation -o slides/index.pdf
```

[collection]: https://app.flus.fr/collections/1743346650628061722
[slides]: https://design.penpot.app/#/workspace/b770c351-4008-11ed-b414-cf2300df1f4f/0cd173d0-4009-11ed-bf98-c67f7655c7b3
[timer]: https://thom4parisot.github.io/talktimer.js/
